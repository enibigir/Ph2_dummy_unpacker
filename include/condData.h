#include "streamer.h"
#include <vector>
#include <map>

class condData{
public:
    void print();
    void Fill(streamer * ss);
    void reset();
    
    uint64_t nCond;
    std::map<uint32_t , uint32_t> cond;
};
 
