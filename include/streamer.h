#pragma once
#include <iostream>
#include <string>
#include <fstream>

class streamer{
public:
    streamer(std::string fName_);
    uint64_t getSubWord(int start, int stop);
    void printWord();
    void printBits();
    void getWord();
    bool endEvent();
    void printEvent();
    uint64_t word_;
private:
    void reorder();
    std::ifstream inputFile_;
    char buffer[100];
    int wordCounter_;
    int fileSize_;
    bool storeEvent_;
    uint64_t *event_;
};
