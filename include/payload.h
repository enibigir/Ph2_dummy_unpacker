#pragma once
#include "streamer.h"
#include "../include/header.h"
#include <vector>

class payload{
public:
    void print();
    void Fill(streamer * ss, const header & h);
    void reset();
    
    std::vector <uint16_t> hitOdd;
    std::vector <uint16_t> hitEven;
    std::vector <uint16_t> stubs;
};
