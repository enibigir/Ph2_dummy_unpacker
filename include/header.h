#pragma once
#include "streamer.h"
#include <vector>

class header{
public:
    void print();
    void Fill(streamer * ss);
    void reset();
    
    // GLOBAL HEADER
    uint8_t BOE_1;
    uint8_t Evt_ty;
    uint32_t LV1_id;
    uint16_t BX_id;
    uint16_t Source_id;
    uint8_t FOV;
    uint8_t misc;
    
    //TK HEADER
    uint8_t   version;
    uint8_t   fmt;
    uint8_t   evtType;
    uint32_t  DTC_status;
    uint16_t  Ncbc;
    uint8_t   FEstatus0;
    uint64_t  FEstatus1;
    
    //CBC data
    std::vector <uint32_t > CBC_status;
    
    
};
