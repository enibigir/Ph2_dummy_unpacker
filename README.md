Small dummy unpacker for the phase 2 tracker upgrade.

The code only works for 2xCBC3 unsparsified data with condition data and can produce files in the "flat ntuple" format.

To build code : mkdir bin; make shoudl do the trick. Root and C++11 required.
Then, to produce ntuples: ./bin/converter.out inFile.daq outFile.root
