#include "include/dummyUnpacker.h"
#include <iostream>
using namespace std;

int main(){
    string fName = "/home/xtaldaq/MagnetTest/DQM/DATA/DAQ/Run_97.daq";

    dummyUnpacker d;
    d.loadFile(fName);
    cout<<"Reading event : "<<endl;

    for (int i = 0; i < 200000; i++){
	d.getEvent();
	if ((i%2000) == 0){
		d.printEvent();
	}
    }
    //d.getEvent();
}
