#include "../include/dummyUnpacker.h"
#include <cstdlib>
using namespace std;

dummyUnpacker::dummyUnpacker(string fName_, string outputFile_):ss(nullptr){
    if (fName_!="")
        ss = new streamer(fName_);
    output_ = new TFile(outputFile_.c_str(),"RECREATE");
    
    t2 = new TTree();
    t2->Branch("hit0",&hit0);
    t2->Branch("hit1",&hit1);
    t2->Branch("stub",&stub);
    t2->Branch("bendCode",&bendCode);
    t2->Branch("bend",&bend);
    t2->Branch("stubSim",&stubSim);
    t2->Branch("bendCodeSim",&bendCodeSim);
    t2->Branch("bendSim",&bendSim);
    t2->Branch("L1A",&L1A);
    t2->Branch("CbcL1A",&CbcL1A);
    t2->Branch("pipeline",&Pipeline);
    t2->Branch("error",&Error);
    t2->Branch("TDC",&TDC);
    t2->Branch("delay_and_group",&delay_and_group);
    t2->Branch("vcth",&vcth);
    t2->Branch("Timestamp",&Timestamp);
//    t2->SetAutoSave(10000000);
    t2->SetDirectory(output_);
}

dummyUnpacker::~dummyUnpacker(){
    output_->Close();
}

void dummyUnpacker::printBuffer(){
    ss->printEvent();
}
void dummyUnpacker::saveEvent(){
    hit0.clear();
    hit1.clear();
    stub.clear();
    bend.clear();
    bendCode.clear();
    stubSim.clear();
    bendCodeSim.clear();
    bendSim.clear();
    Error.clear();
    Pipeline.clear();
    CbcL1A.clear();

    for (auto x:p_.hitEven)
        hit1.push_back((int)x);
    for (auto x:p_.hitOdd)
        hit0.push_back((int)x);
    for (auto s:p_.stubs){
        stub.push_back((254*(s>>12)) + ((s>>4)&0xFF) - 2);
        bendCode.push_back(s&0xF);
    }

    L1A = h_.LV1_id;
    for (auto h_cbc : h_.CBC_status){
        Error.push_back( (h_cbc>>18) & 0x3);
        Pipeline.push_back( (h_cbc>>9) & 0x1FF);
        CbcL1A.push_back( h_cbc & 0x1FF );
    }

    TDC = c_.cond[0xff000003];
    Timestamp = c_.cond[0x81];
    delay_and_group = c_.cond[0xe01];
    vcth = c_.cond[0x5001]<<8+c_.cond[0x4f01];
    t2->Fill();
    
}
void dummyUnpacker::writeFile(){
    t2->Write("flatTree");
}

void dummyUnpacker::printWord(){
    if (ss)
        ss->printWord();
    else
        cerr<<"ERROR, streamer not initialized"<<endl;
}

void dummyUnpacker::printBits(){
    if (ss)
        ss->printWord();
    else
        cerr<<"ERROR, streamer not initialized"<<endl;
}

void dummyUnpacker::loadFile(string fName_){
    if (ss)
        delete ss;
    ss = new streamer(fName_);
    
}
void dummyUnpacker::getWord(){
    if (ss)
        ss->getWord();
    else
        cerr<<"ERROR, streamer not initialized"<<endl;
}

void dummyUnpacker::printEvent(){
    h_.print();
    p_.print();
    c_.print();
}
bool dummyUnpacker::getEvent(){
    h_.Fill(ss);
    p_.Fill(ss,h_);
    c_.Fill(ss);
    return(ss->endEvent());
}
