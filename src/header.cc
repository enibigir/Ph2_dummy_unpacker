 
#include "../include/header.h"
#include <assert.h>
#include <iostream>
using namespace std;

void header::print(){   
    cout<<"BOE_1     = "<<hex<<(int)BOE_1    <<endl;
    cout<<"Evt_ty    = "<<hex<<(int)Evt_ty   <<endl;
    cout<<"LV1_id    = "<<hex<<(int)LV1_id   <<endl;
    cout<<"BX_id     = "<<hex<<(int)BX_id    <<endl;
    cout<<"Source_id = "<<hex<<(int)Source_id<<endl;
    cout<<"FOV       = "<<hex<<(int)FOV      <<endl;
    cout<<"misc      = "<<hex<<(int)misc     <<endl;
    cout<<"version   = "<<hex<<(int)version   <<endl; 
    cout<<"fmt       = "<<hex<<(int)fmt       <<endl; 
    cout<<"evtType   = "<<hex<<(int)evtType   <<endl; 
    cout<<"DTC_status= "<<hex<<(int)DTC_status<<endl; 
    cout<<"Ncbc      = "<<hex<<(int)Ncbc      <<endl; 
    cout<<"FEstatus0 = "<<hex<<(int)FEstatus0 <<endl; 
    cout<<"FEstatus1 = "<<hex<<(int)FEstatus1 <<endl; 
    int cbcId = 0;
    for (auto elem : CBC_status){
        cout<<"CBC"<<cbcId <<" status = "<<elem<<endl;
        cbcId++;
    }
}

void header::reset(){
     BOE_1       = 0;
     Evt_ty      = 0;
     LV1_id      = 0;
     BX_id       = 0;
     Source_id   = 0;
     FOV         = 0;
     misc        = 0;
     version     = 0;
     fmt         = 0;
     evtType     = 0;
     DTC_status  = 0;
     Ncbc        = 0;
     FEstatus0   = 0;
     FEstatus1   = 0;
     CBC_status.clear();
}


void header::Fill(streamer * ss){
    reset();
    ss->getWord();
    //ss->printWord();
    BOE_1     = ss->getSubWord(63,60);
    Evt_ty    = ss->getSubWord(59,56);
    LV1_id    = (uint32_t) ss->getSubWord(55,32);
    BX_id     = ss->getSubWord(31,20);
    Source_id = ss->getSubWord(19, 8);
    FOV       = ss->getSubWord( 7, 4);
    misc      = ss->getSubWord( 3, 0);
    ss->getWord();
    //ss->printWord();
    
    version    =ss->getSubWord(63,60);
    fmt        =ss->getSubWord(59,58);
    evtType    =ss->getSubWord(57,54);
    DTC_status =ss->getSubWord(53,24);
    Ncbc       =ss->getSubWord(23, 8);
    FEstatus0  =ss->getSubWord(7 , 0);
    ss->getWord();
    //ss->printWord();
    FEstatus1  = ss->word_;
    ss->getWord();
    //ss->printWord();

    //CBC data
    int cbcSize;
    assert(fmt < 3);
    if (fmt == 1)
        cbcSize = 20; //Full debug
    else if (fmt==2)
        cbcSize = 1;  //Error mode
    else if (fmt==0)
        cbcSize = 0;  //Summary mode

    int cbcStart = 0;
    int cbcStop  = 0;
    uint32_t cbcWord = 0;
    for (int i = 0; i < Ncbc; i++){
        cbcStop += cbcSize;
        if (cbcStop <= 64){
            cbcWord = ss->getSubWord(63-cbcStart,64-cbcStop);
        }
        else{
            // Stitching two words...
            cbcStop -=64;
            if (cbcStart < 64){
                cbcWord |=ss->getSubWord(63-cbcStart, 0)<<(cbcStop);
                ss->getWord();
                cbcWord |=ss->getSubWord(63,64-cbcStop);
            }
            else {
                cbcStart -=64;
                ss->getWord();
                cbcWord = ss->getSubWord(63-cbcStart,64-cbcStop);
            }
        }
        CBC_status.push_back(cbcWord);
        cbcStart = cbcStop + 1;
    }
}
