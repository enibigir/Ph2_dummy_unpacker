#include "../include/payload.h"
#include <assert.h>
#include <iostream>

using namespace std;


void payload::Fill(streamer * ss, const header & h){
    reset();
    bool is_sparsified = false;
    if ((h.evtType>>2) == 0b01){
        is_sparsified = true;
    }
    else if ((h.evtType>>2) != 0b10){
        cout<<"Fatal error, event type is neither sparsified nor unsparsified !"<<endl;
        cout<<"evtType : "<<h.evtType<<endl;
        exit(-1);
    }
    if (!is_sparsified){
        ss->getWord();
        // First word, has useless stuff... since only 2 or 8 cbc supported, we need to have the following assert : 
        assert (ss->getSubWord(63,48) == 3 || ss->getSubWord(63,48) == 255 || ss->getSubWord(63,48) == 65535);
        
        int stripOffset = 0;
        int startPos    = 16;
        uint64_t data;

        int toRead = h.Ncbc*256;
        while (toRead > 64){
            data = ss->getSubWord(63-startPos,0);
            int dataSize = 64-startPos;
            
            for (int i = 0; i < dataSize; i++){
                bool isHit = (data>>(63-startPos-i))%2;
                if (isHit){
                    int strip = stripOffset +i;
                    if (i%2)
                        hitOdd.push_back(strip/2 - strip/256);
                    else
                        hitEven.push_back(strip/2 -strip/256);
                }
            }
            stripOffset += dataSize;
            startPos     = 0;
            toRead-=dataSize;
            ss->getWord();
        }        
        data = ss->getSubWord(63,64-toRead);
        for (int i = 0; i <= toRead; i++){
            bool isHit = (data>>(toRead-i-1))%2;
            if (isHit){
            int strip = stripOffset +i;
            if (i%2)
                hitOdd.push_back(strip/2-strip/256);
            else
                hitEven.push_back(strip/2-strip/256);
            }
        }
    }
    else{
        int nFE = h.FEstatus0+h.FEstatus1;
        // if more than 1 front-end, you probably should do something smarter...
        assert (nFE == 1);
        ss->getWord();
        int n_clust = ss->getSubWord(63,57);
        int pos_start = 56;
        while (n_clust > 0){
            int pos_stop = pos_start - 14;
            uint16_t clust_word = 0;
            if (pos_stop > -1){
                clust_word = ss->getSubWord(pos_start,pos_stop);
            }
            else{
                clust_word = ss->getSubWord(pos_start,0)<<(-pos_stop);
                ss->getWord();
                clust_word |= ss->getSubWord(63,63+pos_stop);
                pos_stop += 64;
            }
            pos_start = pos_stop - 1;
            if (pos_start == -1){
                ss->getWord();
                pos_start = 63;
            }
            n_clust --;
            int cbc_id  = clust_word >> 11;
            int cl_pos  = (clust_word >> 3) & 0xFF;
            int cl_size = (clust_word ) & 0x7;
            if (cl_pos % 2){
                for (int i = 0; i<= cl_size; i++)
                    hitOdd.push_back((cl_pos>>1)+254*cbc_id+i);
            }
            else{
                for (int i = 0; i<= cl_size; i++)
                    hitEven.push_back((cl_pos>>1)+254*cbc_id+i);
            }

        }
    }
    
    // Now, let's go to stubs.

    ss->getWord();

    int NStubs = ss->getSubWord(63,59);
    int stubSize = 16; // Warning, can change if not 2S module...
    
    assert(ss->getSubWord(58,58)==0);
    int startPos = 57;
    
    while (NStubs > 0){
        int endPos = startPos - stubSize +1;
        if (endPos >= 0){
            stubs.push_back(ss->getSubWord(startPos,endPos));
            startPos = endPos - 1;
        }
        else{
            uint16_t stub = ss->getSubWord(startPos,0)<<(-endPos);
            ss->getWord();
            stub |= ss->getSubWord(63,64+endPos);
            stubs.push_back(stub);
            startPos = 64+endPos-1;
        }
        NStubs --;
    }
}

void payload::print(){
    cout <<"Odd channels :";
    for (auto x: hitOdd)
        cout<<x<<" ";
    cout<<endl;
    cout <<"Even channels :";
    for (auto x: hitEven)
        cout<<x<<" ";
    cout<<endl;
    cout <<"Stubs :";
    for (auto s:stubs)
        cout<<s<<" ";
    cout<<endl;
}

void payload::reset(){
    hitEven.clear();
    hitOdd.clear();
    stubs.clear();
}
